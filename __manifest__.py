# -*- coding: utf-8 -*-

{
    'name': "MPS Planning",

    'summary': """
        custom app to allow production planning""",

    'description': """
        An customized app for production planning.
    """,

    'author': "rintio",
    'website': "http://www.rintio.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/13.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Fabrication',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'mrp'],

    # always loaded
    'data': [
        'views/planning.xml',
    ],
    # only loaded in demonstration mode
    'demo': [],
    'qweb': [],
    'application': True
}
