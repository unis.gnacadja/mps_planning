from odoo import fields , models , api
from datetime import datetime
from datetime import date
from odoo import models
from odoo.addons import project
from odoo import netsvc, tools
from odoo.tools.translate import _
from odoo.exceptions import Warning
from lib2to3.pgen2 import parse
import math
import time
import random
import logging
import os
import os.path
import json
from operator import attrgetter
import json 
import string
import sys
import uuid 


class MrpCycle(models.Model):

    _name = "mrp.planning.cycle"
    _description = "Paramètre de cycle de production"

    name = fields.Char('Nom')
    days = fields.Integer('Duree en jours')


MrpCycle()


class MrpPlanningQueue(models.Model):
    _name = "mrp.planning.queue"
    _description = " planning cycle history"
    cycle_id = fields.Many2one("mrp.planning.cycle", "Période")
    production_id = fields.Many2one("mrp.production", " Objectif de production")
    planning_id = fields.Many2one("mrp.planning", "Programme de production")


MrpPlanningQueue()


class MrpPlanning(models.Model):
    _name = 'mrp.planning'
    _description = "Programme de production"
    name = fields.Char(compute="_compute_name")
    start_date = fields.Date('Date de début')
    end_date = fields.Date('Date de fin')
    status = fields.Boolean('cloture')
    target_ids = fields.One2many('mrp.planning.queue', 'planning_id', 'objectifs de production')

    _defaults = {
        'name': ''
    }

    @api.depends("start_date", "end_date")
    def _compute_name(self):
        for record in self:
            record.name = "Production du " + record.start_date.strftime("%d/%m/%y") + " au " + record.start_date.strftime("%d/%m/%y")


MrpPlanning()
